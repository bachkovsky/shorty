{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad (replicateM)
import Control.Monad.IO.Class (liftIO)
import qualified Data.ByteString.Char8 as BC
import Data.Maybe (isJust)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import qualified Data.Text.Lazy as TL
import qualified Database.Redis as R
import Network.URI (URI, parseURI)
import qualified System.Random as SR
import Web.Scotty

main :: IO ()
main = do
  rConn <- R.connect R.defaultConnectInfo
  scotty 3000 (app rConn)

app :: R.Connection -> ScottyM ()
app rConn = do
  get "/" $ do
    uri <- param "uri"
    let parsedUri = parseURI (TL.unpack uri)
    case parsedUri of
      Just _ -> generateShorty rConn byteUri
        where byteUri = encodeUtf8 (TL.toStrict uri)
      Nothing -> text (shortyAintUri uri)
  get "/:short" $ do
    short <- param "short"
    uri <- liftIO $ getURI rConn short
    case uri of
      Left reply -> text (TL.pack $ show reply)
      Right mbRS ->
        case mbRS of
          Nothing -> text "uri not found"
          Just bs -> html (shortyFound tbs)
            where tbs = TL.fromStrict (decodeUtf8 bs)

generateShorty :: R.Connection -> BC.ByteString -> ActionM ()
generateShorty rConn uri = do
  randomShortLink <- liftIO shortyGen
  let shorty = BC.pack randomShortLink
  shortExists <- liftIO (R.runRedis rConn $ R.exists shorty)
  if True `elem` shortExists
      -- retry when generated value is already present
    then generateShorty rConn uri
    else do
      resp <- liftIO (saveURI rConn shorty uri)
      html (shortyCreated resp randomShortLink)

alphaNum :: String
alphaNum = ['a' .. 'z'] ++ ['A' .. 'Z'] ++ ['0' .. '9']

randomElement :: [a] -> IO a
randomElement xs = do
  let maxIdx = length xs - 1
  randomDigit <- SR.randomRIO (0, maxIdx)
  return (xs !! randomDigit)

shortyGen :: IO String
shortyGen = replicateM 7 (randomElement alphaNum)

saveURI ::
     R.Connection
  -> BC.ByteString
  -> BC.ByteString
  -> IO (Either R.Reply R.Status)
saveURI conn shortURI uri = R.runRedis conn $ R.set shortURI uri

getURI ::
     R.Connection -> BC.ByteString -> IO (Either R.Reply (Maybe BC.ByteString))
getURI conn shortURI = R.runRedis conn $ R.get shortURI

linkShorty :: String -> String
linkShorty shorty =
  "<a href=\"" ++ shorty ++ "\">Copy and paste your short URL</a>"

shortyCreated :: Show a => a -> String -> TL.Text
shortyCreated resp shawty =
  TL.concat [TL.pack (show resp), " shorty is: ", TL.pack (linkShorty shawty)]

shortyAintUri :: TL.Text -> TL.Text
shortyAintUri uri = TL.concat [uri, " wasn't a url"]

shortyFound :: TL.Text -> TL.Text
shortyFound uri = TL.concat ["<a href=\"", uri, "\">", uri, "</a>"]
